﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreManager : MonoBehaviour
{
    public static int score;
    public static int earn;
    public static int more;

    public PlayerHealth pH;
    public int amount = 20;
    public PlayerMovement playerMovement;
    public TimeManage timeManage;

    Text text;


    void Awake ()
    {
        text = GetComponent <Text> ();
        score = 0;
        earn = 0;
        more = 0;
    }


    void Update ()
    {
        text.text = "Score: " + score;
    }

    public void AddScore(int add)
    {
        score += add;
        earn += add;
        more += add;
        if (ScoreManager.earn >= 150)
        {
            pH.currentHealth += amount;
            pH.healthSlider.value = pH.currentHealth;
            EnemyMovement[] enemyMovements = FindObjectsOfType<EnemyMovement>();
            foreach (EnemyMovement eM in enemyMovements)
            {
                eM.freezeTimer = 5f;
                timeManage.gameTime = timeManage.gameTime + 30f;
            }
            earn = 0;
        }

        if (ScoreManager.more >= 200)
        {
            playerMovement.FastTimer = 3f;
            more = 0;
        }
    }
}
