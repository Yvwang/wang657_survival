﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldManage : MonoBehaviour
{
    public PlayerHealth playerHealth;
    public GameObject shield;
    public float spawnTime = 30f;
    public Transform[] spawnPoints;


    // Use this for initialization
    void Start()
    {
        Invoke("Spawn", spawnTime);
    }

    // Update is called once per frame

    void Spawn()
    {
        if (playerHealth.currentHealth <= 0f)
        {
            return;
        }
        int spawnPointIndex = Random.Range(0, spawnPoints.Length);

        Instantiate(shield, spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation);
    }
}