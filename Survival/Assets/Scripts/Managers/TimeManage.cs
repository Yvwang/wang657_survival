﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class TimeManage : MonoBehaviour {
    public float gameTime;
    public PlayerHealth pH;
    public ScoreManager sC;

    Text text;
	
    // Use this for initialization
	void Start () {
        text = GetComponent<Text>();
        gameTime = 300f;
    }
	
	// Update is called once per frame
	void Update () {
        text.text = "Time: " + Mathf.CeilToInt(gameTime) + " sec";
        
        if (pH.isDead == false)
        {
            gameTime = gameTime - Time.deltaTime;
        }

        if (gameTime <= 0f && pH.isDead == false)
        {
            pH.Death();
        }
    }
}